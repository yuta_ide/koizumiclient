FieldSonar-client
====

v1.4がリリースされました. Aチームは[こちら](https://54.64.102.195/#!/A)から利用できます. Bチームは[こちら](http://54.64.102.195/#!/B)から利用できます.

## Demo

![スクリーンショット](./screen_shot.jpeg)

## 機能

#### コメントの投稿

`api/v1/comment` へのPOST

#### 位置情報との結びつけ

HTML5のGeolocation APIを利用することで, 位置情報を取得する.

#### 地図の表示

ピンとコメントが反映されます

#### 双対尺度図の表示

コメントが圧縮された図が表示されます

## benri command

AWSへアクセス
```
ssh -i "koizumiJikkenn.pem.txt" ec2-user@ec2-54-64-102-195.ap-northeast-1.compute.amazonaws.com
```

nodeを有効にする
```
source ~/.nvm/nvm.sh
```

サーバーの開始
```
service httpd start
```

ビルド
```
gulp dist
```

ビルド後ファイルをサーバーに適用
```
cp ~/koizumiPJ/client/dist/{index.html,build.js} /var/www/html
```

## 起動方法

AWS上の[FieldSonar](https://54.64.102.195/#!/A)にアクセスするだけで使えます.

## v1.3からの変更点

+ 次元圧縮モードという名目を, キーワード分布図に変更しました. 

## 課題
* スマホ版が見辛い
